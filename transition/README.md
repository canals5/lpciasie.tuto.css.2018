# **Tutoriel LP CIASIE**

## **Th�me :** CSS Transitions / CSS Multiple columns layout

## **Auteurs** :
- VAN HAAREN Lucas
- LEMMER Richard
- ROBERT Alexandre
- BENIER Hugo


## **Introduction**

CSS Transition sert a �jouter des transitions (d�placement, rotation, agrandissement, ...) a des �l�ments gr�ce a CSS.

CSS Multiple columns layout sert � diviser simplement en colonnes les enfants d'un �l�ments. Cela imite le style des journaux.

## **D�mo**

**Faire la d�mo**

## **Impl�mentation**

### CSS Transitions

Cette fonctionalit� est impl�ment�e par tous les navigateurs les plus connus tels que firefox, chrome, safari hormis Op�ra Mini.
La plus parts des "grands" navigateurs l'impl�mentaient d�ja depuis 2013.

### CSS Multiple columns layout

Cette fonctionnalit� est impl�ment�e par tous les "grands" navigateurs sauf firefox (ainsi que sa version mobile, et op�ra mobile) qui ne le supporte que partiellement: 

`
Partial support refers to not supporting the break-before, break-after, break-inside properties. WebKit- and Blink-based browsers do have equivalent support for the non-standard -webkit-column-break-* properties to accomplish the same result (but only the auto and always values). Firefox does not support break-* but does support the page-break-* properties to accomplish the same result.
`

source: caniuse.com

## **Ressources**

* [**W3C**](https://www.w3schools.com/css/css3_transitions.asp) - CSS Transitions
* [**W3C**](https://www.w3schools.com/css/css3_multiple_columns.asp) - CSS Multiple columns layout
* [**Can I Use ?**](https://caniuse.com/) - Permet de v�rifier l'impl�mentation de la fonctionnalit� dans les diff�rents navigateurs