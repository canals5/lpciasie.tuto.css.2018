# Tutoriel LP CIASIE

## Thème : Fitlering et Blending mode 

## Auteurs :
1. FELIX Paul
2. DUBOUIS Hugo
3. ROHRBACHER Leon
4. MARLY Yanis

## Lien demo :

Filter : https://codepen.io/slywald/pen/yRGBjO?fbclid=IwAR0LvqMxutfQ3THiehVlywX9jQXrrO0Ut7lxMPJe-wvdoOm3BYmmyPyjzPI
Blending mode : https://codepen.io/felix18u/pen/mzjXmy
Blending mode avec 2 images : https://codepen.io/felix18u/pen/MPzYrM

## Tutoriel : 

## Partie 1 : Les filtres

### Introduction
Les filtres permettent d'ajuster le rendu d'une image en la rendant flou, en changeant sont niveau de gris, en changeant sa saturation ect...
En CSS, c'est la propriété 'filter' qui permet d'ajuster notre image. Il existe déjà des fonctions pour gérer le filtre mais on peut trés bien ajouter notre propre filtre au format SVG grâce à la fonction url()

### Syntaxe

La syntaxe pour appliquer un filtre est assez simple mais les fonctions a attribuer peuvent parraïtre plus complexe a mettre en place car elles ont besoin de valeur.

img { 
filter: grayscale(75%);
}

Ici, on applique la valeurs grayscale qui va transformer l'image de 75%.

### Differents types de filtres

#### Grayscale()

Comme nous l'avons vu précédemment, grayscale va permettre de trasnformer l'image en noir et blanc. Il prend en paramètre une valeur en poucentage de 0 à 100. 0% correspond aucun filtre, 100% correspond a une image en noir et blanc.

img {
    filter: grayscale(100%);
}

#### Sepia()

Le filtre sepia se comporte comme grayscale sauf que sa ne sera pas des nuances de gris mais des nuances de bruns.

img {
    filter: sepia(100%);
}

#### Blur()

Blur va permettre de flouter l'image de facon plus ou moins prononcé. Elle prends en paramètre une baleurs en pixel. Plus cette valeur est grande, plus l'image sera flou.

img {
    filter: blur(5px);
}    
#### Saturation()

La saturation définit l'intensité de la couleur. La valeur donnée a cette fonction est défini soit en poucentage soit avec un nombre et n'a pas de limite. En effet, la fonction se comporte comme un multiplicateur. Si on veut par exemple une saturation a 200%, l'intensité de la couleur au été multipliée par 2. Si on donne la valeur 4, l'intensité de la couleur sera multiplié par 4. ect...

img {
    filter: saturation(200%);
    fitler: saturation(4);
}

#### Constrast()

Le contraste ... . Elle se comporte comme la saturation.

img {
    filter: contrast(200%);
    filter: contrast(4);
}

#### Brightness()

La luminosité ... . Elle se comporte aussi comme saturation et contraste

img {
    filter: brightness(200%);
    filter: brightness(4);
}

#### Hue-rotate()

Hue-rotation est assez particulié, Il va modifié les couleurs avec la valeur que l'on lui a donné en degré selon la roue des couleurs. Exemple ...

img {
    filter: hue-rotate(90deg);
    filter: hue-rotate(180deg);
    filter: hue-rotate(240deg);
}

#### Invert()

Comme son nom l'indique, Invert va inveser les couleur de l'image. Comme grayscale et sepia, on lui donne une valeur en poucentage de 0 à 100.

img {
    filter: invert(100%);
}

### Les limites de filter

#### Compatibilité

#### Vitesse de chargement

#### Utilité

## Partie 2 : Le Blend mode

### Introduction

Blend Mode permet de spécifier la manière dont un calque va interagir ou se mélanger (blend) avec un autre. Cet fonctionalités est déjà bien connu des logiciels type Photoshop, GIMP, etc... En fonction du mode choisi, on obtient des effets différents.

### Comment sa marche ?

Les Blend Modes sont les différentes façons de déterminer la couleur finale d'une image quand un élément se superpose, entièrement ou partiellement, à un autre. Ces modes sont des fonctions mathématiques qui utilisent les représentations mathématiques des couleurs (RGB) présentes dans les calques source et background.

### Syntaxe

Il existe 2 cas différents pour appliquer un blending en css. Sur une balise quelconque, on utilisera mix-blend-mode.

img {
mix-blend-mode: multiply;
}

Ou sur un div dont le background est une image. Dans ce cas, on utilisera background-blend-mode

.blend {
background-image : url(img.jpeg);
background-color: red;
background-blend-mode: multiply;
}

Vous pouvez utiliser mix-blend-mode si vous superposez 2 div entre eux que sa soit une image, un fond de couleur, une vidéo ou encore du texte
De plus, si vous mettez le mix-blend-mode sur un, le résultat sera différent que si vous l'attribuez sur l'autre div.
Il existe plusieurs valeurs de blend que je vais vous présenter

### Differents types de blend

#### normal

Retourne l'image sans effet.

#### multiply

Multiplie les couleurs de l'image source et de l'image d'arrière-plan pour obtenir la couleur finale. On obtient une image plus foncée.

#### screen

C'est l'inverse de multiply. Le complément ("1 - valeur de la couleur") de la source et du calque d'arrière-plan sont multipliés.

#### overlay

Applique le mode screen aux pixels plus clairs et le mode multiply aux pixels plus foncés.

#### darken

Applique le ton le plus foncé entre les couleurs de la source et du background.

#### lighten

C'est contraire au précédent. Il applique le ton le plus clair entre les couleurs de la source et du background.

#### color-dodge

Éclaircit la couleur de fond pour refléter la couleur du contenu.

#### color-burn

Assombrit le fond pour refléter la couleur naturelle du contenu.

#### hard-light

C'est l'inverse d'overlay, il applique le mode mutiply aux pixels plus clairs et le mode screen aux pixels plus foncés.

#### soft-light

Utilise le même procédés que overlay mais le fait différement

#### hue

Crée une couleur avec la teinte du contenu combinée à la saturation et à la luminosité de l'arrière-plan.

#### saturation

Crée une couleur à partir de la saturation de la couleur source et la mélange à la teinte et à la luminosité de la couleur d'arrière-plan.

#### color

Produit une couleur à partir de la teinte et de la saturation de la couleur source, et de la luminosité de la couleur d'arrière-plan.

#### luminosity

Produit une couleur à partir de la luminosité de la couleur source, mélangée à la teinte et la saturation de la couleur de background.

#### difference

Soustrait la plus sombre des deux couleurs à la couleur la plus claire, donnant une sorte d'effet “négatif photo”. Le noir n'est pas affecté, le blanc est totalement inversé, et les autres couleurs sont modifiées en fonction de leur niveau de luminosité.

#### exclusion

Produit une version à faible contraste de l'effet produit par le mode difference.