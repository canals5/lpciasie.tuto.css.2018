# Tutoriel LP CIASIE

## Thème : 

Présentation des animations en CSS + Animate.css

## Liens utiles :

* __Exemples Codepen:__ https://codepen.io/collection/XEmRbJ/

## Auteurs :
1. Benjamin  Thouvenin
2. Alexandre Ralli
3. Anthony   Zink
4. Johan     Saltutti
